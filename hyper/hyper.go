package hyper

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/rabbitmq"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/redis"
)

// Type - Type of broker i.e. specify name
type Type string

// Broker - Base literal.
type Broker base.Broker

const (
	// RabbitMQ - Specifies a RabbitMQ Broker Type
	RabbitMQ Type = "RabbitMQ"
	// Redis - Specifies a Redis Broker Type
	Redis Type = "Redis"
)

// New - Creates new MQ Broker
func New(b Type, c *Broker) base.IBroker {
	logger := &logrus.Logger{
		Out:       os.Stderr,
		Formatter: new(logrus.JSONFormatter),
		Hooks:     make(logrus.LevelHooks),
		Level:     logrus.DebugLevel,
	}
	switch b {
	case RabbitMQ:
		return &rabbitmq.Broker{
			Host:        c.Host,
			Port:        c.Port,
			User:        c.User,
			Password:    c.Password,
			Consumers:   []*base.Consumer{},
			Concurrency: c.Concurrency,
			Log:         logger,
			Connection:  nil,
			Channel:     nil,
		}
	case Redis:
		return &redis.Broker{
			Host:        c.Host,
			Port:        c.Port,
			User:        c.User,
			Password:    c.Password,
			Consumers:   []*base.Consumer{},
			Concurrency: c.Concurrency,
			Log:         logger,
			Connection:  nil,
			Ctx:         nil,
		}
	}

	// Returns RabbitMQ broker by default
	return &rabbitmq.Broker{
		Host:        c.Host,
		Port:        c.Port,
		User:        c.User,
		Password:    c.Password,
		Consumers:   []*base.Consumer{},
		Concurrency: c.Concurrency,
		Log:         logger,
		Connection:  nil,
		Channel:     nil,
	}
}
