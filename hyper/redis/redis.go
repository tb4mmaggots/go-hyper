package redis

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
)

// Broker - Literal for Redis
type Broker struct {
	Host        string
	Port        int
	User        string
	Password    string
	Database    int
	Consumers   []*base.Consumer
	Concurrency int
	Log         *logrus.Logger
	Ctx         context.Context
	Connection  *redis.Client
}

func (b *Broker) disconnect() error {
	if err := b.Connection.Close(); err != nil {
		return err
	}
	b.Connection = nil
	return nil
}

func (b *Broker) connect() error {
	if b.Ctx == nil {
		b.Ctx = context.Background()
	}

	if b.Connection == nil {
		b.Connection = redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%v:%v", b.Host, b.Port),
			Username: b.User,
			Password: b.Password, // no password set
			DB:       b.Database, // use default DB
		})
		_, err := b.Connection.Ping(b.Ctx).Result()
		if err != nil {
			return err
		}
	}

	return nil

}

// Publish - Publish a message to the given queue.
func (b *Broker) Publish(exchange string, topic string, content []byte) error {
	if err := b.connect(); err != nil {
		return err
	}
	if err := b.Connection.Publish(b.Ctx, topic, content).Err(); err != nil {
		return err
	}
	return nil
}

// AddConsumer - Adds a consumer for the specified queue
func (b *Broker) AddConsumer(exchange string, topics []string, handler func(msg *base.Message) ([]byte, error), resultTopic *string) error {
	b.Consumers = append(b.Consumers, &base.Consumer{
		ID:          uuid.NewV4().String(),
		Exchange:    exchange,
		Topics:      topics,
		Handler:     handler,
		ResultTopic: resultTopic,
	})
	return nil
}

// Listen - Starts the listener for added consumers.
func (b *Broker) Listen() error {
	_, err := b.Consume(-1)
	if err != nil {
		return err
	}

	return nil
}

// Consume - Consumes messages from broker and exits.
func (b *Broker) Consume(duration int) (*sync.WaitGroup, error) {
	log := b.Log

	if err := b.connect(); err != nil {
		return nil, err
	}
	defer b.disconnect()
	waitForSigTerm := new(sync.WaitGroup)
	waitForSigTerm.Add(1)
	concurrencyLimiter := make(chan *redis.Message, b.Concurrency)

	for _, consumer := range b.Consumers {
		pubsub := b.Connection.PSubscribe(b.Ctx, consumer.Topics...)
		if _, err := pubsub.Receive(b.Ctx); err != nil {
			return nil, err
		}
		defer pubsub.Close()

		messages := pubsub.Channel()
		go func(t *base.Consumer) {
			t.PID = os.Getpid()
			logger := log.WithFields(logrus.Fields{
				"exchange":    t.Exchange,
				"topics":      t.Topics,
				"consumer_id": t.ID,
				"pid":         t.PID,
			})
			logger.Info("consumer started")
			for d := range messages {
				concurrencyLimiter <- d
				payload := []byte(d.Payload)
				go func(exchange string, resultTopic *string, payload []byte) {

					msg := &base.Message{
						Payload: payload,
						Logger:  logger,
					}
					start := time.Now()
					result, err := t.Handler(msg)
					if err != nil {
						logger.Errorf("consumer task execution failed: %v", err)
					}
					duration := time.Since(start)
					logger.Infof("task complete in %v", duration)
					if resultTopic != nil {
						logger.Infof("publishing result to %v", *resultTopic)
						if err := b.Publish(exchange, *resultTopic, result); err != nil {
							logger.Errorf("failed to publish result: %v", err)
						}
					}
					<-concurrencyLimiter
				}(t.Exchange, t.ResultTopic, payload)
			}
		}(consumer)
	}
	if duration > -1 {
		time.Sleep(time.Duration(duration) * time.Second)
		log.Infof("consumer killed after specified %v second(s)", duration)
		waitForSigTerm.Done()

	}
	waitForSigTerm.Wait()
	return waitForSigTerm, nil
}

// Logger - Returns the logger
func (b *Broker) Logger() *logrus.Logger {
	return b.Log
}
