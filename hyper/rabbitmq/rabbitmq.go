package rabbitmq

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
)

// Broker - Literal for RabbitMQ
type Broker struct {
	Host        string
	Port        int
	User        string
	Password    string
	Consumers   []*base.Consumer
	Concurrency int
	Log         *logrus.Logger
	Connection  *amqp.Connection
	Channel     *amqp.Channel
}

func (b *Broker) disconnect() error {
	b.Connection.Close()
	b.Channel.Close()
	b.Connection = nil
	b.Channel = nil
	return nil
}

func (b *Broker) connect() error {
	if b.Connection == nil || b.Connection.IsClosed() {
		conn, err := amqp.Dial(fmt.Sprintf("amqp://%v:%v@%v:%v/", b.User, b.Password, b.Host, b.Port))
		if err != nil {
			return fmt.Errorf("RabbitMQ connection failed: %v", err)
		}
		b.Connection = conn
	}

	if b.Channel == nil {
		ch, err := b.Connection.Channel()
		if err != nil {
			return fmt.Errorf("failed RabbitMQ channel creation: %v", err)
		}
		b.Channel = ch
	}

	return nil
}

// Publish - Publish a message to the given queue.
func (b *Broker) Publish(exchange string, topic string, content []byte) error {
	if err := b.connect(); err != nil {
		return err
	}
	//defer b.disconnect()

	err := b.Channel.ExchangeDeclare(
		exchange,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("could not declare %v exchange: %v", exchange, err)
	}

	//Publish Messages
	err = b.Channel.Publish(
		exchange, // exchange
		topic,    // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(content), //Convert to byte array
		})
	if err != nil {
		return fmt.Errorf("could not puslish to queue %v topic %v: %v", exchange, topic, err)
	}

	return nil
}

// AddConsumer - Adds a consumer for the specified queue
func (b *Broker) AddConsumer(exchange string, topics []string, handler func(msg *base.Message) ([]byte, error), resultTopic *string) error {
	b.Consumers = append(b.Consumers, &base.Consumer{
		ID:          uuid.NewV4().String(),
		Exchange:    exchange,
		Topics:      topics,
		Handler:     handler,
		ResultTopic: resultTopic,
	})
	return nil
}

// Listen - Starts the listener for added consumers.
func (b *Broker) Listen() error {
	_, err := b.Consume(-1)
	if err != nil {
		return err
	}
	return nil
}

// Consume - Consumes tasks from broker and exits.
func (b *Broker) Consume(duration int) (*sync.WaitGroup, error) {
	log := b.Log

	if err := b.connect(); err != nil {
		return nil, err
	}
	defer b.disconnect()
	waitForSigTerm := new(sync.WaitGroup)
	waitForSigTerm.Add(1)
	concurrencyLimiter := make(chan amqp.Delivery, b.Concurrency)
	for _, consumer := range b.Consumers {
		err := b.Channel.ExchangeDeclare(
			consumer.Exchange,
			"topic",
			true,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			return nil, fmt.Errorf("could not declare %v exchange: %v", consumer.Exchange, err)
		}
		queue, err := b.Channel.QueueDeclare(
			strings.Join(consumer.Topics, "_"), // name
			true,                               // durable
			false,                              // delete when unused
			false,                              // exclusive
			false,                              // no-wait
			nil,                                // arguments
		)
		if err != nil {
			return nil, fmt.Errorf("could not declare %v queue: %v", consumer.Queue, err)
		}
		for _, t := range consumer.Topics {
			err = b.Channel.QueueBind(
				queue.Name,        // queue name
				t,                 // routing key
				consumer.Exchange, // exchange
				false,
				nil,
			)
			if err != nil {
				return nil, fmt.Errorf("could not bind %v to topic %v: %v", consumer.Queue, t, err)
			}
		}
		err = b.Channel.Qos(1, 0, false)
		if err != nil {
			return nil, fmt.Errorf("RabbitMQ could not set Qos prefetch: %v", err)
		}

		messages, err := b.Channel.Consume(
			queue.Name,
			"",
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			return nil, fmt.Errorf("RabbitMQ could not register consumer: %v", err)
		}

		go func(t *base.Consumer) {
			t.PID = os.Getpid()
			logger := log.WithFields(logrus.Fields{
				"exchange":    t.Exchange,
				"topics":      t.Topics,
				"consumer_id": t.ID,
				"pid":         t.PID,
			})
			logger.Info("consumer started")
			for d := range messages {
				concurrencyLimiter <- d
				go func(exchange string, resultTopic *string, delivery amqp.Delivery) {
					msg := &base.Message{
						Payload: delivery.Body,
						Logger:  logger,
					}
					start := time.Now()
					result, err := t.Handler(msg)
					if err != nil {
						logger.Errorf("consumer task execution failed: %v", err)
					}
					duration := time.Since(start)
					logger.Infof("task complete in %v", duration)
					if resultTopic != nil {
						logger.Infof("publishing result to %v", *resultTopic)
						if err := b.Publish(exchange, *resultTopic, result); err != nil {
							logger.Errorf("failed to publish result: %v", err)
						}
					}
					if err := delivery.Ack(false); err != nil {
						logger.Errorf("broker message ACK failed:%v", err)
					}
					<-concurrencyLimiter
				}(t.Exchange, t.ResultTopic, d)
			}

		}(consumer)
	}
	if duration > -1 {
		time.Sleep(time.Duration(duration) * time.Second)
		log.Infof("consumer killed after specified %v second(s)", duration)
		waitForSigTerm.Done()

	}
	waitForSigTerm.Wait()
	return waitForSigTerm, nil
}

// Logger - Returns the logger
func (b *Broker) Logger() *logrus.Logger {
	return b.Log
}
