package hyper

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
)

func TestRabbitMQProducer(t *testing.T) {
	t.Parallel()
	host := "localhost"
	port := 5672
	user := "guest"
	password := "guest"

	Convey("RabbitMQ Broker with RabbitMQ Test Server running on localhost:5672 Producer", t, func() {
		b := New(RabbitMQ, &Broker{
			Host:        host,
			Port:        port,
			User:        user,
			Password:    password,
			Concurrency: 1,
		})

		Convey("should publish RabbitMQ message to test_queue queue successfully", func() {
			time.Sleep(1 * time.Second)
			t.Logf("RabbitMQ Produced: %+v", "{\"name\":\"hello rabbit\"}")
			So(b.Publish("dev", "test.rabbit.success", []byte("{\"name\":\"hello rabbit\"}")), ShouldBeNil)
		})
	})
}
func TestRabbitMQConsumer(t *testing.T) {
	t.Parallel()
	host := "localhost"
	port := 5672
	user := "guest"
	password := "guest"

	Convey("RabbitMQ Broker with RabbitMQ Test Server running on localhost:5672 Consumer", t, func() {
		b := New(RabbitMQ, &Broker{
			Host:        host,
			Port:        port,
			User:        user,
			Password:    password,
			Concurrency: 1,
		})

		Convey("should successfully add a consumer & listen for message", func() {
			err := b.AddConsumer("dev", []string{"test.rabbit.#"}, func(msg *base.Message) ([]byte, error) {
				t.Logf("RabbitMQ Consumed: %+v", string(msg.Payload))
				return []byte{}, nil
			}, nil)

			So(err, ShouldBeEmpty)

			Convey("should listen for RabbitMQ message successfully", func() {
				_, err := b.Consume(2)
				So(err, ShouldBeNil)

			})

		})

	})
}

func TestRabbitMQFail(t *testing.T) {
	host := "localhost"
	port := 5671
	user := "guest"
	password := "guest"

	Convey("RabbitMQ Broker with wrong connection port", t, func() {
		b := New(RabbitMQ, &Broker{
			Host:        host,
			Port:        port,
			User:        user,
			Password:    password,
			Concurrency: 1,
		})
		Convey("should successfully add a consumer", func() {
			err := b.AddConsumer("dev", []string{"test.rabbit.fail"}, func(msg *base.Message) ([]byte, error) {
				return []byte{}, nil
			}, nil)

			So(err, ShouldBeEmpty)
		})

		Convey("should fail listening", func() {
			So(string(b.Listen().Error()), ShouldStartWith, "RabbitMQ connection failed")
		})

		Convey("should fail publishing", func() {
			So(string(b.Publish("dev", "test.rabbit.fail", []byte("{\"name\":\"fail_test\"}")).Error()), ShouldStartWith, "RabbitMQ connection failed")
		})

	})
}

func TestRedisProducer(t *testing.T) {
	t.Parallel()
	host := "localhost"
	port := 6379
	Convey("Redis Broker with Redis Test Server running on localhost:6379 Producer", t, func() {
		b := New(Redis, &Broker{
			Host:     host,
			Port:     port,
			User:     "",
			Password: "",
		})

		Convey("should publish for Redis message successfully", func() {
			time.Sleep(1 * time.Second)
			t.Logf("Redis Produced: %+v", "{\"name\":\"hello redis\"}")
			err := b.Publish("dev", "test.redis", []byte("{\"name\":\"hello redis\"}"))
			So(err, ShouldBeNil)

		})

	})
}

func TestRedisConsumer(t *testing.T) {
	t.Parallel()
	host := "localhost"
	port := 6379
	Convey("Redis Broker with Redis Test Server running on localhost:6379", t, func() {
		b := New(Redis, &Broker{
			Host:     host,
			Port:     port,
			User:     "",
			Password: "",
		})

		Convey("should successfully add a consumer", func() {
			err := b.AddConsumer("dev", []string{"test.*"}, func(msg *base.Message) ([]byte, error) {
				t.Logf("Redis Consumed: %+v", string(msg.Payload))
				return []byte{}, nil
			}, nil)

			So(err, ShouldBeEmpty)

			Convey("should listen for Redis message successfully", func() {
				_, err := b.Consume(2)
				So(err, ShouldBeNil)
			})

		})

	})
}

func TestRedisFail(t *testing.T) {
	host := "localhost"
	port := 6378

	Convey("Redis Broker", t, func() {
		b := New(Redis, &Broker{
			Host:     host,
			Port:     port,
			User:     "",
			Password: "",
		})
		Convey("should successfully add a consumer", func() {
			err := b.AddConsumer("dev", []string{"test", "hello"}, func(msg *base.Message) ([]byte, error) {
				return []byte{}, nil
			}, nil)

			So(err, ShouldBeEmpty)
		})

		Convey("should fail listening", func() {
			So(string(b.Listen().Error()), ShouldContainSubstring, "No connection could be made because the target machine actively refused it")
		})

		Convey("should fail publishing", func() {
			So(string(b.Publish("dev", "fail", []byte("{\"name\":\"fail_test\"}")).Error()), ShouldContainSubstring, "No connection could be made because the target machine actively refused it")
		})

	})

}

func TestDefault(t *testing.T) {
	b := New("Random", &Broker{})
	Convey("Default Broker", t, func() {
		Convey("should successfully add a consumer", func() {
			err := b.AddConsumer("dev", []string{"test", "hello"}, func(msg *base.Message) ([]byte, error) {
				return []byte{}, nil
			}, nil)

			So(err, ShouldBeEmpty)
		})

		Convey("should gracefully fail listening with message RabbitMQ connection failed...", func() {
			So(string(b.Listen().Error()), ShouldStartWith, "RabbitMQ connection failed")
		})

		Convey("should gracefully fail publishing with message RabbitMQ connection failed...", func() {
			So(string(b.Publish("dev", "default_fail", []byte{}).Error()), ShouldStartWith, "RabbitMQ connection failed")
		})

	})
}
