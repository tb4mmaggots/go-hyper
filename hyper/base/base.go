package base

import (
	"sync"

	"github.com/sirupsen/logrus"
)

// IBroker - Interface for using MQ Broker
type IBroker interface {
	// Publish - Sends a message on the given queue.
	Publish(exchange string, topic string, content []byte) error
	// AddConsumer - Adds a consumer that will process using the function provided.
	AddConsumer(exchange string, topics []string, handler func(msg *Message) ([]byte, error), resultTopic *string) error
	// Listen - Starts background go-routine listening for messages from broker for specified duration -1 for forever.
	Listen() error
	// Consume - Consumes messages from broker and exits.
	Consume(duration int) (*sync.WaitGroup, error)
	// Logger - Returns the logger.
	Logger() *logrus.Logger
}

// Broker - Base literal.
type Broker struct {
	Host        string
	Port        int
	User        string
	Password    string
	Concurrency int
}

// Message - Type to be passed to handlers.
type Message struct {
	Payload []byte
	Logger  *logrus.Entry
}

//Task - Specifies task properties a consumer will run.
type Consumer struct {
	ID          string
	PID         int
	Queue       string
	Exchange    string
	Topics      []string
	ResultQueue *string
	ResultTopic *string
	Handler     func(msg *Message) ([]byte, error)
}
