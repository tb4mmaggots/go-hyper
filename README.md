# go-hyper

Go based distributed task processing using a broker(Default RabbitMQ)

## Description

This task processor will connect to the broker and consume messages and then run the provided handler function for each queue specified.
Multiple consumers can be added each of which upon initializing the `Listen()` function will run on seperate go routines and consume messages from the broker.
The number of concurrent messages per consumer can be set using the `Concurrency` parameter with the `Broker` setup.
This package has only been tested with RabbitMQ. But other brokers can be added by implementing the `IBroker` interface.

## Quickstart
ex. 
```go
package main

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/tb4mmaggots/go-hyper/hyper"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
)

func main() {
	b := hyper.New(hyper.RabbitMQ, &hyper.Broker{
		Host:        "localhost",
		Port:        5672,
		User:        "guest",
		Password:    "guest",
		Concurrency: 2,
	})

	logger := b.Logger()
	queue := "test_queue"
	resultQueue := "results"
	msg := struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}{
		ID:      uuid.NewV4().String(),
		Message: "Hello World",
	}

	strMsg, err := json.Marshal(msg)
	if err != nil {
		logger.Errorf("failed to marshal message to JSON: %v", err)
	}

	if err := b.Publish(queue, strMsg); err != nil {
		logger.Errorf("failed to publish message: %v", err)
	}

	if err := b.AddConsumer(queue, handleMessage, &resultQueue); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}

	if err := b.AddConsumer(resultQueue, handleMessage, nil); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}

	if err := b.Listen(); err != nil {
		logger.Fatalf("failed to start listening to broker: %v", err)
	}
}

func handleMessage(m *base.Message) ([]byte, error) {
	type Msg struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}

	msg := &Msg{}
	err := json.Unmarshal(m.Payload, msg)
	if err != nil {
		return nil, err
	}

	m.Logger.WithField("message_id", msg.ID).Infof("%v", msg)

	res, err := json.Marshal(&Msg{
		ID:      uuid.NewV4().String(),
		Message: fmt.Sprintf("Message from %v", msg.ID),
	})
	if err != nil {
		return nil, err
	}

	return res, nil
}

```