package main

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/tb4mmaggots/go-hyper/hyper"
	"gitlab.com/tb4mmaggots/go-hyper/hyper/base"
)

func main() {
	// runRedis()
	runRabbit()
}

func runRedis() {
	b := hyper.New(hyper.Redis, &hyper.Broker{
		Host:        "localhost",
		Port:        6379,
		User:        "",
		Password:    "",
		Concurrency: 2,
	})

	logger := b.Logger()
	exchange := "test_queue"
	topics := []string{"test.redis"}
	resultTopic := "results"
	msg := struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}{
		ID:      uuid.NewV4().String(),
		Message: "Hello World",
	}

	strMsg, err := json.Marshal(msg)
	if err != nil {
		logger.Errorf("failed to marshal message to JSON: %v", err)
	}
	for _, t := range topics {
		if err := b.Publish(exchange, t, strMsg); err != nil {
			logger.Errorf("failed to publish message: %v", err)
		}
	}

	if err := b.AddConsumer(exchange, []string{"test.*"}, handleMessage, &resultTopic); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}

	if err := b.AddConsumer(exchange, []string{resultTopic}, handleResult, nil); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}

	err = b.Listen()
	if err != nil {
		logger.Fatalf("failed to start listening to broker: %v", err)
	}
}

func runRabbit() {
	b := hyper.New(hyper.RabbitMQ, &hyper.Broker{
		Host:        "localhost",
		Port:        5672,
		User:        "guest",
		Password:    "guest",
		Concurrency: 2,
	})

	logger := b.Logger()
	exchange := "test_queue"
	topics := []string{"test.rabbit"}
	resultTopic := "results"
	msg := struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}{
		ID:      uuid.NewV4().String(),
		Message: "Hello World",
	}

	strMsg, err := json.Marshal(msg)
	if err != nil {
		logger.Errorf("failed to marshal message to JSON: %v", err)
	}
	for _, t := range topics {
		if err := b.Publish(exchange, t, strMsg); err != nil {
			logger.Errorf("failed to publish message: %v", err)
		}
	}

	if err := b.AddConsumer(exchange, []string{"test.#"}, handleMessage, &resultTopic); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}

	if err := b.AddConsumer(exchange, []string{resultTopic}, handleResult, nil); err != nil {
		logger.Errorf("failed to add consumer: %v", err)
	}
	err = b.Listen()
	if err != nil {
		logger.Fatalf("failed to start listening to broker for 5 seconds: %v", err)
	}
}

func handleMessage(m *base.Message) ([]byte, error) {
	type Msg struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}

	msg := &Msg{}
	err := json.Unmarshal(m.Payload, msg)
	if err != nil {
		return nil, err
	}

	m.Logger.WithField("message_id", msg.ID).Infof("I am a message %+v", msg)

	res, err := json.Marshal(&Msg{
		ID:      uuid.NewV4().String(),
		Message: fmt.Sprintf("Message from %v", msg.ID),
	})
	if err != nil {
		return nil, err
	}

	return res, nil
}

func handleResult(m *base.Message) ([]byte, error) {
	type Msg struct {
		ID      string `json:"id"`
		Message string `json:"message"`
	}

	msg := &Msg{}
	err := json.Unmarshal(m.Payload, msg)
	if err != nil {
		return nil, err
	}

	m.Logger.WithField("message_id", msg.ID).Infof("I am a result %+v", msg)

	res, err := json.Marshal(&Msg{
		ID:      uuid.NewV4().String(),
		Message: fmt.Sprintf("Message from %v", msg.ID),
	})
	if err != nil {
		return nil, err
	}

	return res, nil
}
